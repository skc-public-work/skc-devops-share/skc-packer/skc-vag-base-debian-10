.PHONY: validate build cleanbuid

include .env
export

env_test:
	@echo $(value BOX_FILE_NAME)

validate:
	packer validate .

build:
	packer build .

cleanbuid:
	rm -rf ./build

push_to_nexus:
	cd build && curl -v --user $(NEXUS_USER):$(NEXUS_USER_PW) --upload-file $(BOX_FILE_NAME) https://cad-nexus.ososinfo.org/repository/cad-files/vagrant-boxes/$(BOX_FILE_NAME)
